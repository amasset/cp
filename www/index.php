<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Code postaux v1.4.1</title>
    <style>
        h1 {
            color: red;
        }
    </style>
</head>
<body>
    <h1>Code postaux français v1.4.1</h1>
    <?php
        $filename = 'cp.csv';

        if (file_exists($filename)) {
            $file = fopen($filename, 'r');
            $cp = array();
            while (($line = fgetcsv($file, 1000, ";")) !== FALSE) {
                $cp[] = $line;
            }
            fclose($file);
        } else {
            echo "Le fichier $filename n'existe pas";
        }

        // Supprimons la première ligne de $cp
        array_shift($cp);

        echo '<table>';
        echo '<tr>';
        echo '<th>CP</th>';
        echo '<th>Ville</th>';
        echo '</tr>';

        foreach ($cp as $key => $value) {
            echo '<tr>';
            echo '<td>'.$value[2].'</td>'; 
            echo '<td>'.$value[1].'</td>'; 
            echo '</tr>';
        }

        echo '</table>';
    ?>
</body>
</html>